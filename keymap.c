// -*- compile-command: "cd $HOME/.config/qmk/qmk_firmware && sh util/docker_build.sh crkbd:karloguidoni" -*-
/*
Copyright 2019 @foostan
Copyright 2020 Drashna Jaelre <@drashna>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include QMK_KEYBOARD_H
#include <stdio.h>

/* @see: https://getreuer.info/posts/keyboards/custom-shift-keys/index.html */

#include "features/custom_shift_keys.h"
/* @see: https://getreuer.info/posts/keyboards/autocorrection/index.html */
#include "features/autocorrection.h"
/* @see: https://getreuer.info/posts/keyboards/achordion/index.html */
/* @see: https://www.reddit.com/r/olkb/comments/v5gzvl/qmk_bilateral_combinations_and_ctrlc_ctrlv_ctrlr/ */
#include "features/achordion.h"

/* #include "features/unicodemap.h" */

enum cantor_layers {
    BASE,
    FN1,
    FN2,
    NAV,
};

enum custom_keycodes {
    R_ASSIGN = SAFE_RANGE,
};

/* []{} on zxcv */
#define LSQBKT   KC_LBRC
#define RSQBKT   KC_RBRC
#define LSQCKT   S(KC_LBRC)
#define RSQCKT   S(KC_RBRC)

/* navigation and symbols on external thumb cluster when hold, but*/
/* enter when tap on the left */
#define NAV_SYML LT(NAV, KC_TAB)
/* space when tap on the right */
#define NAV_SYMR LT(NAV, KC_SPC)

#define TAB_GUI  LGUI_T(KC_TAB)
#define CTL_ESC  CTL_T(KC_ESC)
#define CTL_DOT  CTL_T(KC_DOT)
#define ALT_ESC  ALT_T(KC_ESC)

#define CTL_ENT  CTL_T(KC_ENT)
#define NUMB_PAD LT(FN1, KC_BSPC)
#define LALT_BKP LT(KC_LALT, KC_BSPC)
#define FUN_KEYS MO(FN2)

// Left-hand home row mods
#define HOME_A LGUI_T(KC_A)
#define HOME_S LALT_T(KC_S)
#define HOME_D LSFT_T(KC_D)
#define HOME_F LCTL_T(KC_F)
#define HOME_Z LCTL_T(KC_Z)

// Right-hand home row mods
#define HOME_J RCTL_T(KC_J)
#define HOME_K RSFT_T(KC_K)
#define HOME_L LALT_T(KC_L)
#define SL_GUI RGUI_T(KC_SLSH) // I really like having the : instead ; in this position

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
  [BASE] = LAYOUT_split_3x6_3(
//,-------- --------- --------- --------- --------- --------- .                      ,-------- --------- --------- --------- --------- --------- .
    TAB_GUI ,    KC_Q ,    KC_W ,    KC_E ,    KC_R ,    KC_T ,                           KC_Y ,    KC_U ,    KC_I ,    KC_O ,   KC_P  , KC_BSPC ,
//|-------- +-------- +-------- +-------- +-------- +-------- |                      |-------- +-------- +-------- +-------- +-------- +-------- |
    CTL_ESC ,  HOME_A ,  HOME_S ,  HOME_D ,  HOME_F ,    KC_G ,                           KC_H ,  HOME_J ,  HOME_K ,  HOME_L , KC_COLN , KC_DQT  ,
//|-------- +-------- +-------- +-------- +-------- +-------- |                      |-------- +-------- +-------- +-------- +-------- +-------- |
    KC_RSFT ,  HOME_Z ,    KC_X ,    KC_C ,    KC_V ,    KC_B ,                           KC_N ,    KC_M , KC_COMM ,  KC_DOT , SL_GUI  , KC_LSFT ,
//|-------- +-------- +-------- +-------- +-------- +-------- +----------||----------+-------- +-------- +-------- +-------- +-------- +-------- |
                                            ALT_ESC , NAV_SYML, CTL_ENT  ,   NAV_SYMR, NUMB_PAD, FUN_KEYS
                                    //    '------------------------------||------------------------------'
),

[FN1] = LAYOUT_split_3x6_3(
//,-------- --------- --------- --------- --------- --------- .                      ,-------- --------- --------- --------- --------- --------- .
    _______ , KC_LBRC ,    KC_7 ,    KC_8 ,    KC_9 , KC_RBRC ,                        _______ , _______ , _______ , _______ , _______ , _______ ,
//|-------- +-------- +-------- +-------- +-------- +-------- |                      |-------- +-------- +-------- +-------- +-------- +-------- |
    _______ , S(KC_9) ,    KC_4 ,    KC_5 ,    KC_6 , S(KC_0) ,                        _______ , KC_LCTL , KC_LSFT , KC_LALT , KC_LGUI , _______ ,
//|-------- +-------- +-------- +-------- +-------- +-------- |                      |-------- +-------- +-------- +-------- +-------- +-------- |
    _______ ,S(KC_LBRC),   KC_1 ,    KC_2 ,   KC_3 ,S(KC_RBRC),                        KC_PASTE, KC_COPY , _______ , _______ , _______ , _______ ,
//|-------- +-------- +-------- +-------- +-------- +-------- +---------|   |--------+-------- +-------- +-------- +-------- +-------- +-------- |
                                            _______ ,    KC_0 , _______ ,     _______, _______ , _______
                                    //    '-----------------------------|   |----------------------------'
),

[FN2] = LAYOUT_split_3x6_3(
//,-------- --------- --------- --------- --------- --------- .                      ,-------- --------- --------- --------- --------- --------- .
    _______ ,  KC_F12 ,   KC_F7 ,   KC_F8 ,   KC_F9 , _______ ,                        _______ , _______ , _______ , _______ , _______ , _______ ,
//|-------- +-------- +-------- +-------- +-------- +-------- |                      |-------- +-------- +-------- +-------- +-------- +-------- |
    _______ ,  KC_F11 ,   KC_F4 ,   KC_F5 ,   KC_F6 , _______ ,                        _______ , _______ , _______ , _______ , _______ , _______ ,
//|-------- +-------- +-------- +-------- +-------- +-------- |                      |-------- +-------- +-------- +-------- +-------- +-------- |
    _______ ,  KC_F10 ,   KC_F1 ,   KC_F2 ,   KC_F3 , _______ ,                        _______ , _______ , _______ , _______ , _______ , _______ ,
//|-------- +-------- +-------- +-------- +-------- +-------- +---------|   |--------+-------- +-------- +-------- +-------- +-------- +-------- |
                                            _______ ,  KC_F10 , _______ ,     _______, _______ , _______
                                    //    '-----------------------------|   |----------------------------'
),

[NAV] = LAYOUT_split_3x6_3(
//,-------- --------- --------- --------- --------- --------- .                      ,-------- --------- --------- --------- --------- --------- .
    _______ , KC_EXLM ,   KC_AT , KC_HASH ,  KC_DLR , KC_PERC ,                        KC_CIRC , KC_UNDS , KC_MINS ,  KC_EQL ,KC_PPLS  , KC_0    ,
//|-------- +-------- +-------- +-------- +-------- +-------- |                      |-------- +-------- +-------- +-------- +-------- +-------- |
    _______ , KC_ASTR , KC_AMPR , KC_BSLS , KC_PIPE ,S(KC_GRV),                        KC_LEFT , KC_DOWN , KC_UP   , KC_RGHT ,  KC_DQT , _______ ,
//|-------- +-------- +-------- +-------- +-------- +-------- |                      |-------- +-------- +-------- +-------- +-------- +-------- |
    _______ , KC_CIRC , KC_UNDS , KC_MINS , KC_EQL  , KC_PPLS ,                        KC_HOME , KC_PGDN , KC_PGUP , KC_END  , KC_QUOT , _______ ,
//|-------- +-------- +-------- +-------- +-------- +-------- +---------|   |--------+-------- +-------- +-------- +-------- +-------- +-------- |
                                            _______ , KC_GRV  ,S(KC_GRV),    _______ , _______ , _______
                                    //    '-----------------------------|   |----------------------------'
)

};

bool get_tapping_force_hold(uint16_t keycode, keyrecord_t* record) {
    // @see: https://getreuer.info/posts/keyboards/achordion/index.html
    // Enable PERMISSIVE_HOLD!
    // If you quickly hold a tap-hold key after tapping it, the tap action is
    // repeated. Key repeating is useful e.g. for Vim navigation keys, but can
    // lead to missed triggers in fast typing. Here, returning true means we
    // instead want to "force hold" and disable key repeating.
    switch (keycode) {
    // Repeating is useful for Vim navigation keys.
    case HOME_J:
    case HOME_K:
    case HOME_L:
        return false;  // Enable key repeating.
    default:
        return true;  // Otherwise, force hold and disable key repeating.
    }
}

uint16_t achordion_timeout(uint16_t tap_hold_keycode) {
  return 500;
}

/* Define exceptions for bilateral combinations of achordion lib */
/* @see: https://getreuer.info/posts/keyboards/achordion/index.html */
bool achordion_chord(uint16_t tap_hold_keycode,
                     keyrecord_t* tap_hold_record,
                     uint16_t other_keycode,
                     keyrecord_t* other_record) {

  // Exceptionally consider the following chords as holds, even though they
  // are on the same hand.

  switch (tap_hold_keycode) {


  // left hand
  // R3 left pinky
  case TAB_GUI:
      if (
          other_keycode == KC_Q ||
          other_keycode == KC_W ||
          other_keycode == KC_E ||
          other_keycode == KC_R ||
          other_keycode == KC_T ||
          other_keycode == HOME_A ||
          other_keycode == HOME_S ||
          other_keycode == HOME_D ||
          other_keycode == HOME_F ||
          other_keycode == KC_G ||
          other_keycode == HOME_Z ||
          other_keycode == KC_X ||
          other_keycode == KC_C ||
          other_keycode == KC_V ||
          other_keycode == KC_B ||
          other_keycode == CTL_ENT ||
          other_keycode == ALT_ESC
          ) {return true; }
      break;

  // R2 left pinky
  case CTL_ESC:
      if (
          other_keycode == KC_Q ||
          other_keycode == KC_W ||
          other_keycode == KC_E ||
          other_keycode == KC_R ||
          other_keycode == KC_T ||
          other_keycode == HOME_A ||
          other_keycode == HOME_S ||
          other_keycode == HOME_D ||
          other_keycode == HOME_F ||
          other_keycode == KC_G ||
          other_keycode == HOME_Z ||
          other_keycode == KC_X ||
          other_keycode == KC_C ||
          other_keycode == KC_V ||
          other_keycode == CTL_ENT ||
          other_keycode == KC_B
          ) {return true; }
      break;

  // left middle thumb
  case CTL_ENT:
      if (
          other_keycode == KC_Q ||
          other_keycode == KC_W ||
          other_keycode == KC_E ||
          other_keycode == KC_R ||
          other_keycode == KC_T ||
          other_keycode == HOME_A ||
          other_keycode == HOME_S ||
          other_keycode == HOME_D ||
          other_keycode == HOME_F ||
          other_keycode == KC_G ||
          other_keycode == HOME_Z ||
          other_keycode == KC_X ||
          other_keycode == KC_C ||
          other_keycode == KC_V ||
          other_keycode == KC_B ||
          other_keycode == CTL_ENT
          ) { return true; }
      break;

  // R1 left pinky
  case HOME_Z:
      if (
          other_keycode == HOME_S ||
          other_keycode == HOME_D ||
          other_keycode == HOME_F ||
          other_keycode == KC_G ||
          other_keycode == KC_X ||
          other_keycode == KC_C ||
          other_keycode == KC_V ||
          other_keycode == KC_B
          ) { return true; }
      break;

  // right hand
  // right out thumb
  case NAV_SYMR:
      if (
          other_keycode == KC_Y ||
          other_keycode == KC_U ||
          other_keycode == KC_I ||
          other_keycode == KC_O ||
          other_keycode == KC_P ||
          other_keycode == KC_BSPC ||
          other_keycode == KC_H ||
          other_keycode == HOME_J ||
          other_keycode == HOME_K ||
          other_keycode == HOME_L ||
          other_keycode == KC_COLN ||
          other_keycode == KC_COMM ||
          other_keycode == KC_DOT ||
          other_keycode == SL_GUI
          ) { return true; }
      break;

  // right middle thumb
  case NUMB_PAD:
      if (
          other_keycode == KC_Y ||
          other_keycode == KC_U ||
          other_keycode == KC_I ||
          other_keycode == KC_O ||
          other_keycode == KC_P ||
          other_keycode == KC_BSPC ||
          other_keycode == KC_H ||
          other_keycode == HOME_J ||
          other_keycode == HOME_K ||
          other_keycode == HOME_L ||
          other_keycode == KC_COLN
          ) { return true; }
      break;

  }

  // Also allow same-hand holds when the other key is in the rows below the
  // alphas. I need the `% (MATRIX_ROWS / 2)` because my keyboard is split.
  if (other_record->event.key.row % (MATRIX_ROWS / 2) >= 3) { return true; }

  // Otherwise, follow the opposite hands rule.
  return achordion_opposite_hands(tap_hold_record, other_record);

}

/* @see: https://getreuer.info/posts/keyboards/custom-shift-keys/index.html */
const custom_shift_key_t custom_shift_keys[] = {
  {KC_DQT,  KC_QUOT }, // Shift " is '
  {KC_COLN, KC_SCLN}, // Shift : is ;
};

uint8_t NUM_CUSTOM_SHIFT_KEYS =
    sizeof(custom_shift_keys) / sizeof(custom_shift_key_t);

enum combo_events {
  // . and C => activate Caps Word.
  CAPS_COMBO,
  COMBO_LENGTH
};
uint16_t COMBO_LEN = COMBO_LENGTH;

const uint16_t caps_combo[] PROGMEM = {KC_COMM, KC_C, COMBO_END};

combo_t key_combos[] = {
  [CAPS_COMBO] = COMBO_ACTION(caps_combo),
};

void process_combo_event(uint16_t combo_index, bool pressed) {
  if (pressed) {
    switch(combo_index) {

      case CAPS_COMBO:
        caps_word_on();  // Activate Caps Word.
        break;

    }
  }
}

bool process_record_user(uint16_t keycode, keyrecord_t* record) {
    if (!process_achordion(keycode, record)) { return false; }
    if (!process_custom_shift_keys(keycode, record)) { return false; }
    if (!process_autocorrection(keycode, record)) { return false; }
    switch (keycode) {
    case R_ASSIGN:
        if (record->event.pressed) {
            // when keycode R_ASSIGN is pressed
            SEND_STRING(" <-");
        } else {
            // when keycode R_ASSIGN is released
        }
        break;
    }
    return true;
}

void matrix_scan_user(void) {
  achordion_task();
}
