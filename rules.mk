MOUSEKEY_ENABLE = yes    # Mouse keys
# RGBLIGHT_ENABLE = yes    # Enable WS2812 RGB underlight.
# OLED_ENABLE     = yes
# OLED_DRIVER     = SSD1306
LTO_ENABLE      = yes

# TAP_DANCE_ENABLE  = yes
# UNICODE_ENABLE    = no  # Unicode (can't be used with unicodemap)
# UNICODEMAP_ENABLE = yes # Enable extended unicode
EXTRAKEY_ENABLE   = yes
COMMAND_ENABLE    = no
COMBO_ENABLE      = yes
CAPS_WORD_ENABLE  = yes
SRC += features/custom_shift_keys.c
SRC += features/autocorrection.c
SRC += features/achordion.c
